"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

require("babel-polyfill");

var test = function test() {
	return new Promise(function (resolve, reject) {
		setTimeout(function () {
			resolve('ok');
		}, 3000);
	});
};

exports.default = test;
